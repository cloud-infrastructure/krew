#!/bin/bash

set -x
set -e

SPEC=$(ls -1 *.spec | head -1)
PKG=$(rpm -q --specfile ${SPEC} --queryformat "%{name}-%{version}\n")
commit=$(cat $SPEC | grep "%global commit" | awk '{print $3}')

cwd=$(pwd)
rm -rf $PKG
git clone https://github.com/kubernetes-sigs/krew.git $PKG
cd $PKG
git checkout "${commit}"
go mod vendor

cd "${cwd}"
tar czf $PKG.tar.gz $PKG


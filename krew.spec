%global with_unit_test 0
%if 0%{?fedora}
%global with_devel   0
%global with_bundled 0
%global with_debug   0
%else
%global with_devel   0
%global with_bundled 0
%global with_debug   0
%endif

%if 0%{?with_debug}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif


%global provider        github
%global provider_tld    com
%global project         kubernetes-sigs
%global repo            krew
# https://github.com//kubernetes-sigs/krew
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     k8s.io/kubernetes-sigs
%global commit          6fcdb794c532d3f2849bb4a8a942a19c09ef3002
%global shortcommit     %(c=%{commit}; echo ${c:0:7})

%global krew_version            0.4.2
%global krew_git_version        v%{krew_version}

##############################################

Name:           krew
Version:        %{krew_version}
Release:        2%{?dist}
Summary:        Krew is the package manager for kubectl plugins.
License:        ASL 2.0
URL:            https://%{provider_prefix}
Source0:        https://%{provider_prefix}/archive/%{commit}/%{repo}-%{krew_version}.tar.gz

BuildRequires: gcc
BuildRequires: golang

Requires: git
Requires: kubernetes-client

%description
%{summary}

%if 0%{?with_devel}
%package devel
Summary:       %{summary}
BuildArch:     noarch


%description devel
%{summary}

This package contains library source intended for
building other packages which use import path with
%{import_path} prefix.
%endif

%prep
%setup -n %{name}-%{krew_version}

%build
export GIT_COMMIT=%{commit}
export GIT_TAG=%{krew_git_version}
_LDFLAGS="            -X sigs.k8s.io/krew/internal/version.gitCommit=${GIT_COMMIT}"
export _LDFLAGS="${_LDFLAGS} -X sigs.k8s.io/krew/internal/version.gitTag=${GIT_TAG}"
cd cmd/krew
go build -ldflags "${_LDFLAGS}" .

%install
install -D -p -m 0755 cmd/krew/krew %{buildroot}%{_bindir}/krew
# install completion
install -d -m 0755 %{buildroot}%{_datadir}/bash-completion/completions/
%{buildroot}%{_bindir}/krew completion bash > %{buildroot}%{_datadir}/bash-completion/completions/krew

%files
%{_bindir}/krew
%{_datadir}/bash-completion/completions/krew

%changelog
* Thu Dec 02 2021 Spyros Trigazis <spyridon.trigazis@cern.ch> - 0.4.2-2
- CI, bash completion

* Wed Dec 01 2021 Spyros Trigazis <spyridon.trigazis@cern.ch> - 0.4.2-1
- update to https://github.com/kubernetes-sigs/krew/releases/tag/v0.4.2

* Thu Feb 25 2021 Spyros Trigazis <spyridon.trigazis@cern.ch> - 3.5.2-1
- update to 3.5.2

* Tue May 26 2020 Spyros Trigazis <spyridon.trigazis@cern.ch> - 2.16.7-2.el7.cern
- Fix CI for: Update to v2.16.7 OS-11523

* Mon May 25 2020 Spyros Trigazis <spyridon.trigazis@cern.ch> - 2.16.7-1.el7.cern
- Update to v2.16.7 OS-11523

* Fri Nov 23 2018 Spyros Trigazis <strigazi@gmail.com> - 2.11.0-5.el7.cern
- Fix version with VERSION, GIT_COMMIT, GIT_DIRTY, BuildMetadata.
  OS-8102

* Mon Nov 05 2018 Spyros Trigazis <strigazi@gmail.com> - 2.11.0-4.el7.cern
- Build in openstackclients7-queens.

* Wed Oct 17 2018 Spyros Trigazis <strigazi@gmail.com> - 2.11.0-2.el7.cern
- Build in cci7-tags to build with golang 1.11

* Mon Sep 17 2018 Spyros Trigazis <strigazi@gmail.com>
- Initial package
